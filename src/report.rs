use crate::errors::HelixError;
use crate::slice::ByteSlice;

/// Represents a report unit. This takes the string version of the slice (along with the config)
/// from the reader, updates itself and produces the corresponding unit of report.
pub trait Report {
    type Slice;
    type Config;
    type Error: From<HelixError> + Send + 'static;
    type ByteSlice: ByteSlice<Slice = Self::Slice, Error = Self::Error> + Send + 'static;

    /// Create a new instance of this report.
    fn new(headers: Vec<Vec<u8>>, config: Self::Config) -> Self;

    /// Update this report with a slice.
    fn update(&mut self, slice: Self::ByteSlice) -> Result<(), Self::Error>;
}
