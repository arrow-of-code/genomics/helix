use crate::errors::{HelixError, HelixResult};

use std::fs::{self, File};
use std::io::BufReader;
use std::path::{Path, PathBuf};

lazy_static! {
    /// Available number of CPUs in this machine.
    pub static ref NUM_CORES: usize = num_cpus::get();
}

/// 64 KB chunks for efficient buffered reading.
pub const BUFFER_CAPACITY: usize = 64 * 1024;

/// Minimum number of slices to be sent to worker threads.
pub const SLICES_PER_CHUNK: usize = 20;
/// Number of chunks to be queued in threads.
pub const CHUNKS_PER_THREAD: usize = 10;

/// Generic config for all reporting programs (conforms to builder pattern).
pub struct GenericConfig {
    /// Input file path.
    pub file_path: PathBuf,
    /// Size of the input file in bytes.
    pub size: usize,
    /// Number of threads to spawn for analysis (optional).
    /// By default, it goes for available CPUs in machine.
    pub threads: usize,
    /// Output file path (if any). By default, it takes the input
    /// file path with JSON extension.
    pub out_path: PathBuf,
    /// Number of slices per chunk.
    pub slices_per_chunk: usize,
    /// Number of chunks per thread. Beyond this, the main thread will
    /// wait for the queue to be consumed. This is particularly useful
    /// for machines with less CPUs.
    pub chunks_per_thread: usize,
    /// A buffered reader initialized for the input file.
    reader: Option<BufReader<File>>,
}

impl GenericConfig {
    /// Create the config for this input file.
    pub fn for_input_file<P: AsRef<Path>>(input_file: P) -> HelixResult<GenericConfig> {
        let file_path = input_file.as_ref().to_owned();
        let fd = File::open(&file_path)?;
        let meta = fd.metadata()?;
        let mut out_path = file_path.clone();
        out_path.set_extension("json");

        Ok(GenericConfig {
            file_path,
            reader: Some(BufReader::with_capacity(BUFFER_CAPACITY, fd)),
            size: meta.len() as usize,
            threads: *NUM_CORES,
            out_path,
            slices_per_chunk: SLICES_PER_CHUNK,
            chunks_per_thread: CHUNKS_PER_THREAD,
        })
    }

    /// Set the number of threads for the program.
    pub fn threads(&mut self, threads: Option<usize>) -> &mut Self {
        match threads {
            Some(j) if j > 0 => self.threads = j,
            _ => self.threads = *NUM_CORES,
        }

        self
    }

    /// Set the output path for this program. It creates an empty
    /// file to evaluate whether the path is writable.
    pub fn output<P: AsRef<Path>>(&mut self, path: Option<P>) -> HelixResult<&mut Self> {
        if let Some(p) = path.as_ref() {
            let path = p.as_ref().to_owned();
            if self.file_path == path {
                return Err(HelixError::SameInputOutput);
            }

            // Create and remove file to see if we have write access.
            File::create(&path)?;
            fs::remove_file(&path).expect("expected to remove file");
            self.out_path = path;
        } else {
            self.out_path = self.file_path.clone();
            self.out_path.set_extension("json");
        }

        Ok(self)
    }

    /// Set the number of slices per chunk.
    pub fn slices(&mut self, slices: Option<usize>) -> &mut Self {
        match slices {
            Some(s) if s > SLICES_PER_CHUNK => self.slices_per_chunk = s,
            _ => self.slices_per_chunk = SLICES_PER_CHUNK,
        }

        self
    }

    /// Set the number of chunks per thread.
    pub fn chunks(&mut self, chunks: Option<usize>) -> &mut Self {
        match chunks {
            Some(c) if c > CHUNKS_PER_THREAD => self.chunks_per_thread = c,
            _ => self.chunks_per_thread = CHUNKS_PER_THREAD,
        }

        self
    }

    /// Set the capacity for the underlying buffer. The given value
    /// should be at least 1 KB for this to have an effect.
    ///
    /// Panics if the reader has already been taken.
    pub fn buffer_capacity(&mut self, cap: Option<usize>) -> &mut Self {
        match cap {
            Some(c) if c > 1024 => {
                let reader = self.take_reader().into_inner();
                self.reader = Some(BufReader::with_capacity(c, reader));
            }
            _ => (),
        }

        self
    }

    /// One-time function to get the underlying reader.
    /// Panics if the reader has already been obtained.
    pub(crate) fn take_reader(&mut self) -> BufReader<File> {
        self.reader.take().expect("cannot get underlying reader")
    }

    /// Sets the reader back.
    pub(crate) fn set_reader(&mut self, r: BufReader<File>) {
        self.reader = Some(r);
    }
}
