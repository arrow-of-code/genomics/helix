use chrono::{offset::Utc, SecondsFormat};
use env_logger::Builder;
use log::LevelFilter;

use std::env;
use std::io::Write;
use std::time::Instant;

/// Prepares the logger with the universal datetime format and INFO level.
pub fn prepare_logger() {
    let mut builder = Builder::new();
    builder
        .format(|buf, record| {
            write!(
                buf,
                "{}: {}: {}\n",
                Utc::now().to_rfc3339_opts(SecondsFormat::Secs, true),
                record.level(),
                record.args()
            )
        })
        .filter_level(LevelFilter::Info);
    if env::var("LOG_LEVEL").is_ok() {
        builder = Builder::from_env("LOG_LEVEL");
    }

    builder.init();
}

/// Timer to be used throughout dependent projects.
pub struct Timer(Instant);

impl Timer {
    /// Start this timer.
    #[inline]
    pub fn start() -> Self {
        Timer(Instant::now())
    }

    /// Elapsed time in nanoseconds since the start of this timer.
    #[inline]
    pub fn elapsed_nanos(&self) -> u64 {
        let duration = self.0.elapsed();
        duration.as_secs() * 1000000000 + duration.subsec_nanos() as u64
    }

    /// Elapsed time in seconds since the start of this timer.
    #[inline]
    pub fn elapsed_secs(&self) -> f32 {
        self.elapsed_nanos() as f32 / 1000000000.
    }
}
