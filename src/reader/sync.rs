use crate::slice::ByteSlice;

use crossbeam_channel::{Receiver, Sender, TryRecvError};
use std::iter::Enumerate;
use std::ops::AddAssign;
use std::thread::JoinHandle;
use std::vec::IntoIter as VecOwnedIter;
use std::{cmp, mem};

/// An iterator over the chunk of slices sent to a thread. It provides
/// the line number and the slice.
pub struct Slices<S> {
    slice_count: usize,
    iter: Enumerate<VecOwnedIter<S>>,
    /// Flag to indicate whether this is an empty slice.
    /// (which indicates a cancellation message to running threads).
    pub is_empty: bool,
}

impl<S> Slices<S> {
    /// Create an instance from a vector of values.
    /// `slice_count` indicates the number of values collected so far.
    pub fn from(vec: Vec<S>, slice_count: usize) -> Self {
        Slices {
            slice_count,
            is_empty: vec.is_empty(),
            iter: vec.into_iter().enumerate(),
        }
    }

    /// Create an empty iterator.
    pub fn empty() -> Self {
        Slices {
            slice_count: 0,
            is_empty: true,
            iter: vec![].into_iter().enumerate(),
        }
    }
}

impl<S> Iterator for Slices<S>
where
    S: ByteSlice,
{
    type Item = (usize, S);

    fn next(&mut self) -> Option<(usize, S)> {
        self.iter.next().map(|(i, slice)| {
            let lines = slice.get_metrics().lines();
            (self.slice_count * lines + i * lines + 1, slice)
        })
    }
}

/// A collection of senders which send items in a batch
/// to running threads in round-robin fashion.
pub struct SliceSender<S, E> {
    slice_count: usize,
    slices_per_chunk: usize,
    error_receiver: Receiver<E>,
    idx: usize,
    jobs: usize,
    slices: Vec<S>,
    inner: Vec<Sender<Slices<S>>>,
}

impl<S, E> SliceSender<S, E>
where
    S: ByteSlice,
{
    /// Initialize a chunk sender with a vector of senders.
    /// - `slices_per_chunk` indicates the number of slices to be sent in a chunk.
    /// - `error_receiver` receives errors from anlyzers.
    pub fn new(
        senders: Vec<Sender<Slices<S>>>,
        slices_per_chunk: usize,
        error_receiver: Receiver<E>,
    ) -> Self {
        SliceSender {
            slice_count: 0,
            slices_per_chunk,
            error_receiver,
            idx: 0,
            jobs: senders.len(),
            inner: senders,
            slices: Vec::with_capacity(slices_per_chunk),
        }
    }

    /// Adds a slice to the current chunk. If the chunk has grown to
    /// its threshold, then it's sent to the next thread in round-robin.
    /// If we've received an error, then this method will return that error.
    pub fn add_slice(&mut self, slice: S) -> Result<(), E> {
        self.slices.push(slice);
        self.slice_count += 1;

        if let Ok(e) = self.error_receiver.try_recv() {
            for sender in &self.inner {
                let _ = sender.send(Slices::empty());
            }

            return Err(e);
        }

        if self.slices.len() == self.slices_per_chunk {
            self.send();
        }

        Ok(())
    }

    /// Check for errors from analyzers (if any).
    pub fn check_errors_if_any(&self) -> Result<bool, E> {
        match self.error_receiver.try_recv() {
            Ok(e) => Err(e),
            Err(TryRecvError::Disconnected) => Ok(false),
            Err(TryRecvError::Empty) => Ok(true),
        }
    }

    /// Flush the existing chunk of slices (if any).
    pub fn flush(&mut self) {
        if !self.slices.is_empty() {
            self.send();
        }

        for sender in &self.inner {
            let _ = sender.send(Slices::empty());
        }
    }

    /// Send the slice chunk to the current thread in round-robin.
    fn send(&mut self) {
        let slices = mem::replace(&mut self.slices, Vec::with_capacity(self.slices_per_chunk));
        // Otherwise, this will panic due to overflowing subtraction (for slices < slices_per_chunk)
        let count = self.slice_count - cmp::min(self.slice_count, self.slices_per_chunk);
        let slices = Slices::from(slices, count);
        let _ = self.inner[self.idx].send(slices);
        self.idx += 1;
        self.idx %= self.jobs;
    }
}

/// This is used to join a list of thread handles and then merge the values
/// returned by those handles.
pub struct Collector<R> {
    /// List of handles to the spawned threads.
    pub handles: Vec<JoinHandle<R>>,
}

impl<R> Collector<R>
where
    R: AddAssign,
{
    /// Get the values from each handle and merges them one at a time.
    /// Panics if there are no handles.
    pub fn collect_all(self) -> R {
        let mut object: Option<R> = None;
        for handle in self.handles {
            let r = handle.join().expect("unexpected panic in thread");
            if object.is_none() {
                object = Some(r);
                continue;
            }

            if let Some(old) = object.as_mut() {
                *old += r;
            }
        }

        object.expect("bleh")
    }
}
