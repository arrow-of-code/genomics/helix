mod sync;

use crate::config::GenericConfig;
use crate::errors::HelixError;
use crate::report::Report;
use crate::slice::ByteSlice;

use crossbeam_channel::{self as channel, Receiver, Sender};
use std::fmt::Display;
use std::io::{self, Write};
use std::mem;
use std::ops::AddAssign;
use std::thread::{self, JoinHandle};

pub use self::sync::{Collector, SliceSender, Slices};

/// Reader trait implemented by all file readers. The whole point of this is to keep
/// file reading in the same thread. It gets a `ByteSlice` from the buffered
/// reader, collects them into a chunk, and sends the chunk to the analyzer thread.
/// We also ensure that there's a balance between reading and analyzing i.e., we don't
/// keep sending the chunks blindly (as it'll consume more memory).
///
/// NOTE: If used properly, this implementation will never panic.
pub trait Reader {
    type Slice;
    type ReportConfig: Clone + Send + 'static;
    type Error: Display + From<HelixError> + Send + 'static;
    type ByteSlice: ByteSlice<Slice = Self::Slice, Error = Self::Error> + Send + 'static;

    /// Launch an analyzer thread with a progress and error sender.
    /// Progress sender notifies once a chunk is exhausted.
    /// Error sender notifies when the report generates an error.
    /// (when the thread is also shut down).
    fn launch_analyzer<R>(
        headers: Vec<Vec<u8>>,
        config: Self::ReportConfig,
        receiver: Receiver<Slices<Self::ByteSlice>>,
        error_sender: Sender<Self::Error>,
    ) -> JoinHandle<R>
    where
        R: Report<
                Config = Self::ReportConfig,
                Error = Self::Error,
                Slice = Self::Slice,
                ByteSlice = Self::ByteSlice,
            > + Send
            + 'static,
    {
        thread::spawn(move || {
            let mut report = R::new(headers, config);
            loop {
                if let Ok(slices) = receiver.recv() {
                    if slices.is_empty {
                        // Empty slice indicates cancellation.
                        mem::drop(receiver); // force disconnect
                        break;
                    }

                    for (line, slice) in slices {
                        if let Err(e) = report.update(slice) {
                            // Log the error with line number.
                            error!("Error in line {}: {}", line, e);
                            let _ = error_sender.send(e);
                            break;
                        }
                    }
                }
            }

            report
        })
    }

    /// Create analyzers which consume the slices from the reader,
    /// and collect the handles into a `Collector`.
    fn spawn_analyzers<R>(
        headers: Vec<Vec<u8>>,
        reader_config: &GenericConfig,
        report_config: Self::ReportConfig,
    ) -> (SliceSender<Self::ByteSlice, Self::Error>, Collector<R>)
    where
        R: Report<
                Config = Self::ReportConfig,
                Error = Self::Error,
                Slice = Self::Slice,
                ByteSlice = Self::ByteSlice,
            > + Send
            + 'static,
    {
        let jobs = reader_config.threads;
        let (etx, erx) = channel::unbounded::<Self::Error>();
        let mut senders = vec![];
        let mut handles = vec![];

        for _ in 0..jobs {
            let (tx, rx) = channel::bounded(reader_config.chunks_per_thread);
            senders.push(tx);
            let handle =
                Self::launch_analyzer(headers.clone(), report_config.clone(), rx, etx.clone());
            handles.push(handle);
        }

        let sender = SliceSender::new(senders, reader_config.slices_per_chunk, erx);
        (sender, Collector { handles })
    }

    /// Start reading from the given `BufReader` and send the slices through the MPSC sender.
    fn start_reader(
        config: &mut GenericConfig,
        sender: SliceSender<Self::ByteSlice, Self::Error>,
    ) -> JoinHandle<Result<(), Self::Error>> {
        let file_size = config.size;
        let reader = config.take_reader();
        let mut jobs = config.threads;
        thread::spawn(move || -> Result<(), Self::Error> {
            let stdout = io::stdout();
            let mut reader = reader;
            let mut sender = sender;
            let mut progress_shown = -1;
            let mut processed = 0;
            if file_size == 0 {
                return Err(Self::Error::from(HelixError::EmptyFile));
            }

            loop {
                let slice = match Self::ByteSlice::get_slice(&mut reader)? {
                    Some(s) => s,
                    None => {
                        // We've reached EOF
                        sender.flush(); // send the rest (if any)
                        sender.check_errors_if_any()?;
                        break;
                    }
                };

                let info = slice.get_metrics();
                processed += info.bytes();
                sender.add_slice(slice)?;

                let progress = (processed as f32 * 100.0 / file_size as f32).round() as i16;
                if progress > progress_shown {
                    // only print when we've made progress
                    print!("\rProgress: {}%", progress);
                    progress_shown = progress;
                    let _ = stdout.lock().flush();
                }
            }

            while jobs > 0 {
                let possible = sender.check_errors_if_any()?;
                if !possible {
                    jobs -= 1; // disconnected
                }
            }

            Ok(())
        })
    }

    fn start<R>(
        reader_config: &mut GenericConfig,
        report_config: Self::ReportConfig,
    ) -> Result<R, Self::Error>
    where
        R: Report<
                Config = Self::ReportConfig,
                Error = Self::Error,
                Slice = Self::Slice,
                ByteSlice = Self::ByteSlice,
            > + AddAssign
            + Send
            + 'static,
    {
        let mut reader = reader_config.take_reader();
        let mut slice = Self::ByteSlice::new();
        let headers = match slice.get_file_headers(&mut reader) {
            Ok(h) => h,
            Err(None) => return Err(Self::Error::from(HelixError::EmptyFile)),
            Err(Some(e)) => return Err(e),
        };

        let (sender, collector) = Self::spawn_analyzers(headers, &*reader_config, report_config);
        reader_config.set_reader(reader);

        let read_handle = Self::start_reader(reader_config, sender);
        read_handle
            .join()
            .expect("expected reader to join thread")?;
        Ok(collector.collect_all())
    }
}
