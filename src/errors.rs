#[macro_export]
macro_rules! impl_err_from {
    ($err:ident :: $type:ty > $variant:ident) => {
        impl From<$type> for $err {
            fn from(s: $type) -> Self {
                $err::$variant(s)
            }
        }
    };
}

/// Generic result used throughout this library.
pub type HelixResult<T> = Result<T, HelixError>;

/// Global error which encapsulates all related errors.
#[derive(Debug, Fail)]
pub enum HelixError {
    #[fail(display = "I/O error: {}", _0)]
    Io(std::io::Error),
    #[fail(display = "Input and output file paths are same.")]
    SameInputOutput,
    #[fail(display = "File is empty.")]
    EmptyFile,
}

impl_err_from!(HelixError::std::io::Error > Io);
