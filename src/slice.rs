use crate::errors::HelixError;

use std::io::BufRead;
use std::ops::AddAssign;

/// Macro to convert various fields in the struct implementing `ByteSlice`
/// to a UTF-8 checked string.
#[macro_export]
macro_rules! string_from_bytes {
    ($bytes:expr) => {{
        if let Some(b'\n') = $bytes.last().map(|b| *b) {
            $bytes.pop().unwrap();
            if let Some(b'\r') = $bytes.last().map(|b| *b) {
                $bytes.pop().unwrap();
            }
        }

        String::from_utf8($bytes)?
    }};
}

/// Struct to keep track of bytes and lines in a slice.
#[derive(Clone, Copy, Default)]
pub struct ReadInfo {
    /// Number of bytes read.
    bytes: usize,
    /// Number of lines read.
    lines: usize,
}

impl ReadInfo {
    #[inline]
    pub fn bytes(&self) -> usize {
        self.bytes
    }

    #[inline]
    pub fn lines(&self) -> usize {
        self.lines
    }
}

impl AddAssign for ReadInfo {
    fn add_assign(&mut self, other: Self) {
        self.bytes += other.bytes;
        self.lines += other.lines;
    }
}

/// Trait to represent the byte slice variant (i.e., it should only contain raw bytes).
/// This ensures that the main thread doesn't do anything other than reading the file. All kinds
/// of validation (including UTF-8 check) are done in child threads.
pub trait ByteSlice: Sized {
    type Slice;
    type Error: From<HelixError>;

    /// Create a new instance of this slice (called only before reading).
    fn new() -> Self;

    /// Get the number of bytes read while filling the slice.
    fn get_metrics(&self) -> ReadInfo;

    /// Validate this byte slice and convert it to a string slice. This should be
    /// called only in reporting (i.e., in a child thread).
    fn validate(self) -> Result<Self::Slice, Self::Error>;

    /// Get the next line from the reader into a byte vector.
    /// `fill_slice` should call this method to fill a particular field in the `ByteSlice` struct.
    fn eat_line<B: BufRead>(
        buf: &mut Vec<u8>,
        reader: &mut B,
    ) -> Result<ReadInfo, Option<Self::Error>> {
        let n = reader
            .read_until(b'\n', buf)
            .map_err(|e| Some(HelixError::from(e).into()))?;
        if n == 0 {
            // represents EOF - this slice is either erroneous or empty.
            return Err(None);
        }

        Ok(ReadInfo { bytes: n, lines: 1 })
    }

    /// Get the headers of this file. A file can contain the headers only
    /// in the first few lines. This method is called only once i.e., before
    /// getting the actual content from the file, and the slice will be empty.
    ///
    /// **NOTE:** The implementor should consume and return all the headers.
    /// Since there's no way peek, it's also possible to consume a line that's
    /// probably not a header, the implementor should go ahead and consume the
    /// whole slice.
    fn get_file_headers<B: BufRead>(
        &mut self,
        reader: &mut B,
    ) -> Result<Vec<Vec<u8>>, Option<Self::Error>>;

    /// Fill this slice with data fed by a buffered reader.
    fn fill_slice<B: BufRead>(&mut self, reader: &mut B) -> Result<(), Option<Self::Error>>;

    /// Get a single slice from the reader.
    fn get_slice<B: BufRead>(reader: &mut B) -> Result<Option<Self>, Self::Error> {
        let mut slice = Self::new();
        match slice.fill_slice(reader) {
            Ok(_) => Ok(Some(slice)),
            Err(None) => Ok(None),
            Err(Some(e)) => Err(e),
        }
    }
}
