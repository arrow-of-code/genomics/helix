#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate helix;

use crossbeam_channel as channel;
use helix::config::GenericConfig;
use helix::errors::HelixError;
use helix::reader::{Reader, Slices};
use helix::report::Report;
use helix::slice::{ByteSlice, ReadInfo};

use std::fs::{self, File};
use std::io::{BufRead, Write};
use std::ops::AddAssign;
use std::string::FromUtf8Error;

#[derive(Clone)]
pub struct Slice<B> {
    line1: B,
    line2: B,
    info: ReadInfo,
}

// Mock with an error enum - this is how errors are usually represented.
#[derive(Debug, Fail)]
pub enum SomeError {
    #[fail(display = "Internal error: {}", _0)]
    Helix(HelixError),
    #[fail(display = "UTF-8 error: {}", _0)]
    Utf8(FromUtf8Error),
    #[fail(display = "foobar")]
    Foobar,
}

impl_err_from!(SomeError::HelixError > Helix);
impl_err_from!(SomeError::FromUtf8Error > Utf8);

impl ByteSlice for Slice<Vec<u8>> {
    type Slice = Slice<String>;
    type Error = SomeError;

    fn new() -> Self {
        Slice {
            line1: vec![],
            line2: vec![],
            info: ReadInfo::default(),
        }
    }

    fn get_metrics(&self) -> ReadInfo {
        self.info
    }

    fn get_file_headers<B: BufRead>(
        &mut self,
        _: &mut B,
    ) -> Result<Vec<Vec<u8>>, Option<Self::Error>> {
        Ok(vec![])
    }

    fn validate(mut self) -> Result<Self::Slice, Self::Error> {
        Ok(Slice {
            line1: string_from_bytes!(self.line1),
            line2: string_from_bytes!(self.line2),
            info: self.info,
        })
    }

    fn fill_slice<B: BufRead>(&mut self, reader: &mut B) -> Result<(), Option<Self::Error>> {
        self.info += Self::eat_line(&mut self.line1, reader)?;
        self.info += Self::eat_line(&mut self.line2, reader)?;
        Ok(())
    }
}

#[derive(Clone)]
pub struct SomeReportConfig(usize);

pub struct SomeReport {
    headers: Vec<Vec<u8>>,
    raise_error_on_slice: usize,
    called: usize,
}

impl AddAssign for SomeReport {
    fn add_assign(&mut self, other: Self) {
        self.called += other.called;
    }
}

impl Report for SomeReport {
    type Config = SomeReportConfig;
    type Error = SomeError;
    type Slice = Slice<String>;
    type ByteSlice = Slice<Vec<u8>>;

    fn new(headers: Vec<Vec<u8>>, config: Self::Config) -> Self {
        SomeReport {
            headers,
            raise_error_on_slice: config.0,
            called: 0,
        }
    }

    fn update(&mut self, _slice: Self::ByteSlice) -> Result<(), Self::Error> {
        self.called += 1;
        if self.called == self.raise_error_on_slice {
            return Err(SomeError::Foobar); // for simulating error
        }
        Ok(())
    }
}

pub struct SomeReader;

impl Reader for SomeReader {
    type ReportConfig = SomeReportConfig;
    type Error = SomeError;
    type Slice = Slice<String>;
    type ByteSlice = Slice<Vec<u8>>;
}

/* <----- Actual tests from here -----> */

#[test]
fn test_analyzer() {
    let (etx, erx) = channel::unbounded::<SomeError>();
    let (tx, rx) = channel::unbounded();
    let slice = Slice {
        // create a sample slice
        line1: vec![65, 66, 67],
        line2: vec![65, 66, 67],
        info: ReadInfo::default(),
    };

    // create the iterator
    let slices = Slices::from(vec![slice; 3], 0);
    let _ = tx.send(slices); // send the slices to the analyzer
    let _ = tx.send(Slices::empty()); // send cancel message.
    let handle = SomeReader::launch_analyzer::<SomeReport>(vec![], SomeReportConfig(0), rx, etx);
    let r = handle.join().unwrap();
    assert_eq!(r.called, 3); // called for 3 slices
    erx.recv().unwrap_err(); // no errors
}

#[test]
fn test_analyzer_error_forward() {
    let (etx, erx) = channel::unbounded::<SomeError>();
    let (tx, rx) = channel::unbounded();
    let slice = Slice {
        // create a sample slice
        line1: vec![65, 66, 67],
        line2: vec![65, 66, 67],
        info: ReadInfo::default(),
    };

    let slices = Slices::from(vec![slice; 4], 0);
    let _ = tx.send(slices);
    let _ = SomeReader::launch_analyzer::<SomeReport>(vec![], SomeReportConfig(4), rx, etx);
    match erx.recv().unwrap() {
        SomeError::Foobar => (),
        _ => unreachable!("expected error to propagate from report"),
    }
}

#[test]
fn test_analyzer_spawn() {
    File::create("booya").unwrap();
    let config = GenericConfig::for_input_file("booya").unwrap();
    fs::remove_file("booya").unwrap();
    let (_, collector) =
        SomeReader::spawn_analyzers::<SomeReport>(vec![], &config, SomeReportConfig(0));
    assert_eq!(collector.handles.len(), config.threads);
}

#[test]
fn test_reader_run_and_flush() {
    let mut fd = File::create("booya1").unwrap();
    let mut config = GenericConfig::for_input_file("booya1").unwrap();
    config.threads(Some(1));
    let bytes = b"FOO\nBAR\nFOO\nBAR\nFOO\nBAR\nFOO\nBAR\nFOO\nBAR"; // 5 slices (per our def.)
    config.size = bytes.len(); // just a rough estimate to suppress `HelixError::InvalidSize`
    config.slices_per_chunk = 4;
    let (sender, collector) = SomeReader::spawn_analyzers::<SomeReport>(
        vec![Vec::from(b"ABC" as &[_]), (b"abc" as &[_]).into()],
        &config,
        SomeReportConfig(0),
    );
    fd.write_all(bytes as &[u8]).unwrap();

    let handle = SomeReader::start_reader(&mut config, sender);
    handle.join().unwrap().unwrap(); // successful completion
    fs::remove_file("booya1").unwrap();
    let report = collector.collect_all();
    assert_eq!(report.headers, vec![vec![65, 66, 67], vec![97, 98, 99]]);
    // First 4 slices will be pushed to chunk and sent to the analyzer thread.
    // Next chunk will have a single slice, and even though it won't be sent
    // automatically by the sender, we're flushing the sender when we finish reading.
    // This will send the remaining slices.
    assert_eq!(report.called, 5); // TL;DR: all 5 slices should've been received.
}

#[test]
fn test_reader_invalid_size() {
    let mut fd = File::create("booya2").unwrap();
    let mut config = GenericConfig::for_input_file("booya2").unwrap();
    let bytes = b"FOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR";
    let (sender, _) =
        SomeReader::spawn_analyzers::<SomeReport>(vec![], &config, SomeReportConfig(0));
    fd.write_all(bytes as &[u8]).unwrap();
    let handle = SomeReader::start_reader(&mut config, sender);
    match handle.join().unwrap() {
        Err(SomeError::Helix(HelixError::EmptyFile)) => (),
        _ => unreachable!("expected size error"),
    }

    fs::remove_file("booya2").unwrap();
}

#[test]
fn test_reader_error_forward() {
    let mut fd = File::create("booya3").unwrap();
    let mut config = GenericConfig::for_input_file("booya3").unwrap();
    let bytes = b"FOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR";
    config.size = bytes.len();
    // configure the report to raise an error on the third slice.
    let (sender, _) =
        SomeReader::spawn_analyzers::<SomeReport>(vec![], &config, SomeReportConfig(3));
    fd.write_all(bytes as &[u8]).unwrap();
    let handle = SomeReader::start_reader(&mut config, sender);
    match handle.join().unwrap() {
        Err(SomeError::Foobar) => (),
        _ => unreachable!("expected custom error"),
    }

    fs::remove_file("booya3").unwrap();
}
