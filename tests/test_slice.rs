#[macro_use]
extern crate failure_derive;
#[macro_use]
extern crate helix;

use crossbeam_channel as channel;
use helix::errors::HelixError;
use helix::reader::{Collector, SliceSender};
use helix::slice::{ByteSlice, ReadInfo};

use std::io::{BufRead, BufReader};
use std::string::FromUtf8Error;
use std::thread;

#[derive(Clone)]
struct Slice<B> {
    line1: B,
    line2: B,
    info: ReadInfo,
}

// Mock with an error enum - this is how errors are usually represented.
#[derive(Debug, Fail)]
enum SomeError {
    #[fail(display = "Internal error: {}", _0)]
    Helix(HelixError),
    #[fail(display = "UTF-8 error: {}", _0)]
    Utf8(FromUtf8Error),
    #[fail(display = "foobar")]
    Foobar,
}

impl_err_from!(SomeError::HelixError > Helix);
impl_err_from!(SomeError::FromUtf8Error > Utf8);

impl ByteSlice for Slice<Vec<u8>> {
    type Slice = Slice<String>;
    type Error = SomeError;

    fn new() -> Self {
        Slice {
            line1: vec![],
            line2: vec![],
            info: ReadInfo::default(),
        }
    }

    fn get_metrics(&self) -> ReadInfo {
        self.info
    }

    fn get_file_headers<B: BufRead>(
        &mut self,
        _: &mut B,
    ) -> Result<Vec<Vec<u8>>, Option<Self::Error>> {
        Ok(vec![])
    }

    fn validate(mut self) -> Result<Self::Slice, Self::Error> {
        Ok(Slice {
            line1: string_from_bytes!(self.line1),
            line2: string_from_bytes!(self.line2),
            info: self.info,
        })
    }

    fn fill_slice<B: BufRead>(&mut self, reader: &mut B) -> Result<(), Option<Self::Error>> {
        self.info += Self::eat_line(&mut self.line1, reader)?;
        self.info += Self::eat_line(&mut self.line2, reader)?;
        Ok(())
    }
}

#[test]
fn test_slice() {
    let mut buffer = BufReader::new(b"FOOBARASASE\nFOOBAR\n" as &[u8]);
    let slice = Slice::get_slice(&mut buffer).unwrap().unwrap();
    assert_eq!(slice.get_metrics().bytes(), 19);
    assert_eq!(slice.get_metrics().lines(), 2);
    assert!(*slice.line1.last().unwrap() == b'\n'); // new line at the end of slice
    let new_slice = slice.validate().unwrap();
    assert!(new_slice.line1.chars().last().unwrap() != '\n'); // new line has been removed by macro
    let final_slice = Slice::get_slice(&mut buffer).unwrap();
    assert!(final_slice.is_none()); // no more slices!
}

#[test]
fn test_slice_sender() {
    let (etx, erx) = channel::unbounded::<SomeError>(); // error channel
    let (tx1, rx1) = channel::unbounded(); // job 1 channel
    let (tx2, rx2) = channel::unbounded(); // job 2 channel
    let bytes = b"FOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR\nFOOBAR";
    let mut buffer = BufReader::new(bytes as &[u8]);
    // 2 jobs, 2 slices at a time (per job)
    let mut sender = SliceSender::new(vec![tx1, tx2], 2, erx);

    /* Slices are sent and received properly */

    for _ in 0..2 {
        // push two slices
        let slice = Slice::get_slice(&mut buffer).unwrap().unwrap();
        sender.add_slice(slice).unwrap();
    }

    let mut slices = rx1.recv().unwrap(); // job 1 receives an iterator of 2 slices
    let (line, slice) = (&mut slices).next().unwrap();
    assert_eq!(slice.line1, b"FOOBAR\n" as &[u8]);
    assert_eq!(slice.line2, b"FOOBAR\n" as &[u8]);
    assert_eq!(line, 1); // Make sure that line numbers match the start of the slice
    let (line, _) = (&mut slices).next().unwrap();
    assert_eq!(line, 3);

    for _ in 0..2 {
        // next 2 slices will be sent to job 2
        let slice = Slice::get_slice(&mut buffer).unwrap().unwrap();
        sender.add_slice(slice).unwrap();
    }

    let mut slices = rx2.recv().unwrap();
    let (line, _) = (&mut slices).next().unwrap();
    assert_eq!(line, 5);
    let (line, _) = (&mut slices).next().unwrap();
    assert_eq!(line, 7);

    /* Errors in slices send cancellation message to all receivers */

    let _ = etx.send(SomeError::Foobar); // send an error
    let slice = Slice::get_slice(&mut buffer).unwrap().unwrap();
    sender.add_slice(slice).unwrap_err(); // next add will trigger a cancellation.
    assert!(rx1.recv().unwrap().is_empty); // both jobs get an empty slice.
    assert!(rx2.recv().unwrap().is_empty);

    let mut buffer = BufReader::new(bytes as &[u8]);
    let slice = Slice::get_slice(&mut buffer).unwrap().unwrap();
    for _ in 0..10 {
        // push 10 more slices
        sender.add_slice(slice.clone()).unwrap();
    }

    /* Flushing distributes all remaining slices to threads */

    for _ in 0..3 {
        // make sure job 1 has received the remaining 3 iterators of 2 slices)
        let _ = rx1.recv().unwrap();
    }

    // Add 2 more slices
    sender.add_slice(slice.clone()).unwrap();
    sender.add_slice(slice.clone()).unwrap();

    sender.flush();
    let slice = rx1.recv().unwrap();
    // iterator has only one slice (it should have "2", but this was the effect of our flush)
    assert_eq!(slice.collect::<Vec<_>>().len(), 1);
}

#[test]
fn test_collector() {
    let collector = Collector {
        // Spawn 4 threads, each computing something and resulting '2'
        handles: (0..4).map(|_| thread::spawn(|| 2)).collect(),
    };

    assert_eq!(collector.collect_all(), 8); // 2 + 2 + 2 + 2
}
