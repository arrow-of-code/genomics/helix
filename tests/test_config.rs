use helix::config::GenericConfig;
use helix::errors::HelixError;
use rand::Rng;

use std::fs::{self, File};
use std::path::PathBuf;

// Tests are ran in parallel - same will always create racing conditions.
fn random_file_name() -> String {
    rand::thread_rng().gen_ascii_chars().take(10).collect()
}

#[test]
fn test_config_creation() {
    let ref name = random_file_name();
    let cpus = num_cpus::get();
    File::create(name).unwrap();
    let config = GenericConfig::for_input_file(name).unwrap();
    assert_eq!(config.file_path, PathBuf::from(name));
    assert_eq!(config.size, 0);
    assert_eq!(config.threads, cpus);
    assert_eq!(config.slices_per_chunk, 20);
    assert_eq!(config.chunks_per_thread, 10);
    assert_eq!(config.out_path, PathBuf::from(name.clone() + ".json"));
    fs::remove_file(name).unwrap();
}

#[test]
fn test_config_change() {
    let cpus = num_cpus::get();
    let ref name = random_file_name();
    File::create(name).unwrap();
    let mut config = GenericConfig::for_input_file(name).unwrap();
    assert_eq!(config.threads, cpus);
    config.threads(Some(0)); // "zero" threads makes no sense!
    assert_eq!(config.threads, cpus);
    config.threads(Some(2));
    assert_eq!(config.threads, 2);
    config.slices(Some(50));
    assert_eq!(config.slices_per_chunk, 50);
    config.chunks(Some(15));
    assert_eq!(config.chunks_per_thread, 15);

    config.output::<String>(None).unwrap(); // no effect
    assert_eq!(config.out_path, PathBuf::from(name.clone() + ".json"));
    match config.output(Some(name)) {
        Err(HelixError::SameInputOutput) => (), // same name causes error
        _ => unreachable!(),
    }

    fs::remove_file(name).unwrap();
}
